# json-schemas

- File swp-devtools.schema.json is schema file for swp.json file

    For some reason, the value of the "$schema" property must start with http and not https. If you use https, schema validation will not work in Visual Studio.